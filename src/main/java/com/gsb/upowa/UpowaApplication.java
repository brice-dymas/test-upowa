package com.gsb.upowa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UpowaApplication {

    public static void main(String[] args) {
        SpringApplication.run(UpowaApplication.class, args);
    }

}
