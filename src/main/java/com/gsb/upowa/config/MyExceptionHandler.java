package com.gsb.upowa.config;

import com.gsb.upowa.exception.ContractAlreadyExistsException;
import com.gsb.upowa.exception.ContractNumberException;
import com.gsb.upowa.exception.PhoneNumberException;
import com.gsb.upowa.util.EndPointResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

/**
 * Projet :  upowa
 * Package :  com.gsb.upowa.config
 * File :  MyExceptionHandler
 * Created on : 2021, Saturday 25 of December
 * Created at : 1:30 PM
 * Author name : Brice dymas
 * Author's mail : briceguemkam@gmail.com / brice.guemkam@iforce5.com
 */
@ControllerAdvice
public class MyExceptionHandler {
    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<EndPointResponse> handleGlobalExceptions(HttpServletRequest request, Exception ex) {
        EndPointResponse response = EndPointResponse.buildErrorResponse(ex.getMessage(), request.getRequestURI());

        ex.printStackTrace();

        if (ex instanceof PhoneNumberException || ex instanceof ContractNumberException) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        } else if (ex instanceof ContractAlreadyExistsException) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(response);
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
    }

}
