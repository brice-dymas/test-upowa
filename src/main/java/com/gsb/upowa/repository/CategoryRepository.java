package com.gsb.upowa.repository;

import com.gsb.upowa.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Projet :  upowa
 * Package :  com.gsb.upowa.repository
 * File :  CategoryRepository
 * Created on : 2021, Saturday 25 of December
 * Created at : 12:50 PM
 * Author name : Brice dymas
 * Author's mail : briceguemkam@gmail.com / brice.guemkam@iforce5.com
 */
public interface CategoryRepository extends JpaRepository<Category, Long> {
}
