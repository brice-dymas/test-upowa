package com.gsb.upowa.repository;

import com.gsb.upowa.dto.ListCustomerDTO;
import com.gsb.upowa.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * Projet :  upowa
 * Package :  com.gsb.upowa.repository
 * File :  CustomerRepository
 * Created on : 2021, Saturday 25 of December
 * Created at : 12:49 PM
 * Author name : Brice dymas
 * Author's mail : briceguemkam@gmail.com / brice.guemkam@iforce5.com
 */
public interface CustomerRepository extends JpaRepository<Customer, Long> {

    boolean existsByContractNumber(@NotBlank String contractNumber);

    @Query("SELECT new com.gsb.upowa.dto.ListCustomerDTO(c.names, c.phone, c.contractNumber, c.route, c.createdDate) from Customer c order by c.createdDate desc ")
    List<ListCustomerDTO> getAllCustomers();
}
