package com.gsb.upowa.endpoint;

import com.gsb.upowa.dto.AddCustomerDTO;
import com.gsb.upowa.exception.ContractAlreadyExistsException;
import com.gsb.upowa.exception.ContractNumberException;
import com.gsb.upowa.exception.PhoneNumberException;
import com.gsb.upowa.model.Customer;
import com.gsb.upowa.services.inter.CustomerService;
import com.gsb.upowa.util.EndPointResponse;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.gsb.upowa.util.EndPointResponse.buildSuccessResponse;

/**
 * Projet :  upowa
 * Package :  com.gsb.upowa.endpoint
 * File :  CustomerController
 * Created on : 2021, Saturday 25 of December
 * Created at : 1:34 PM
 * Author name : Brice dymas
 * Author's mail : briceguemkam@gmail.com / brice.guemkam@iforce5.com
 */
@RestController
@RequestMapping("/api/customer")
@RequiredArgsConstructor
public class CustomerController {

    private final CustomerService customerService;
    private final ModelMapper modelMapper = new ModelMapper();

    @GetMapping("/list")
    public ResponseEntity<EndPointResponse> getAllCustomers() {
        return ResponseEntity.ok(buildSuccessResponse(customerService.getAllCustomers()));
    }

    @PostMapping("/create")
    public ResponseEntity<EndPointResponse> createCustomer(@RequestBody AddCustomerDTO dto) {
        Customer toSave = modelMapper.map(dto, Customer.class);
        if (toSave.getPhone() == null || toSave.getPhone().isBlank() || toSave.getPhone().length() != 9)
            throw new PhoneNumberException(toSave.getPhone());

        if (toSave.getContractNumber() == null || toSave.getContractNumber().isBlank() || toSave.getContractNumber().length() < 1)
            throw new ContractNumberException();

        if (customerService.existsByContractNumber(toSave.getContractNumber()))
            throw new ContractAlreadyExistsException(toSave.getContractNumber());

        toSave = customerService.save(toSave);
        return ResponseEntity.status(HttpStatus.CREATED).body(buildSuccessResponse(toSave));
    }
}
