package com.gsb.upowa.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Projet :  upowa
 * Package :  com.gsb.upowa.model
 * File :  Category
 * Created on : 2021, Saturday 25 of December
 * Created at : 12:43 PM
 * Author name : Brice dymas
 * Author's mail : briceguemkam@gmail.com / brice.guemkam@iforce5.com
 */
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Category {

    @Id
    @GeneratedValue
    private Long id;

    private String name;
}
