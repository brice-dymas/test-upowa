package com.gsb.upowa.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

/**
 * Projet :  upowa
 * Package :  com.gsb.upowa.model
 * File :  Customer
 * Created on : 2021, Saturday 25 of December
 * Created at : 12:45 PM
 * Author name : Brice dymas
 * Author's mail : briceguemkam@gmail.com / brice.guemkam@iforce5.com
 */
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Customer {

    @Id
    @GeneratedValue
    private Long id;

    @NotBlank
    private String names;
    @NotBlank
    private String phone;
    @NotBlank
    @Column(name = "contract_number", unique = true)
    private String contractNumber;
    private String route;

    @Builder.Default
    private LocalDateTime createdDate = LocalDateTime.now();

    @PrePersist
    void setCreationDate() {
        createdDate = LocalDateTime.now();
    }
}
