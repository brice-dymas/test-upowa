package com.gsb.upowa.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Projet :  upowa
 * Package :  com.gsb.upowa.exception
 * File :  ContractNumberException
 * Created on : 2021, Saturday 25 of December
 * Created at : 1:25 PM
 * Author name : Brice dymas
 * Author's mail : briceguemkam@gmail.com / brice.guemkam@iforce5.com
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class ContractNumberException extends RuntimeException {
    /**
     * Constructs a new runtime exception with the specified detail message.
     * The cause is not initialized, and may subsequently be initialized by a
     * call to {@link #initCause}.
     */
    public ContractNumberException() {
        super("Le numero de contrat ne doit pas etre vide");
    }
}
