package com.gsb.upowa.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Projet :  upowa
 * Package :  com.gsb.upowa.exception
 * File :  PhoneNumberException
 * Created on : 2021, Saturday 25 of December
 * Created at : 1:21 PM
 * Author name : Brice dymas
 * Author's mail : briceguemkam@gmail.com / brice.guemkam@iforce5.com
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class PhoneNumberException extends RuntimeException {
    /**
     * Constructs a new runtime exception with the specified detail message.
     * The cause is not initialized, and may subsequently be initialized by a
     * call to {@link #initCause}.
     *
     * @param phoneNumber the detail message. The detail message is saved for
     *                    later retrieval by the {@link #getMessage()} method.
     */
    public PhoneNumberException(String phoneNumber) {
        super(String.format("Le numero de telephone %s doit contenir exactement 09 chiffres", phoneNumber));
    }
}
