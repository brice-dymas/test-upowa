package com.gsb.upowa.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Projet :  upowa
 * Package :  com.gsb.upowa.exception
 * File :  ResourceNotFoundException
 * Created on : 2021, Saturday 25 of December
 * Created at : 12:56 PM
 * Author name : Brice dymas
 * Author's mail : briceguemkam@gmail.com / brice.guemkam@iforce5.com
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException {
    String entityName;
    String columnName;
    Object columnValue;

    /**
     * Constructs a new runtime exception with {@code null} as its
     * detail message.  The cause is not initialized, and may subsequently be
     * initialized by a call to {@link #initCause}.
     */
    public ResourceNotFoundException() {
        super("La ressource demandee n'a pu etre trouvee");
    }

    /**
     * Constructs a new runtime exception with the specified detail message.
     * The cause is not initialized, and may subsequently be initialized by a
     * call to {@link #initCause}.
     *
     * @param entityName the detail message. The detail message is saved for
     *                   later retrieval by the {@link #getMessage()} method.
     */
    public ResourceNotFoundException(String entityName, String columnName, Object columnValue) {
        super("La ressource " + entityName + "." + columnName + " = " + columnValue + " n'existe pas!");
        this.entityName = entityName;
        this.columnName = columnName;
        this.columnValue = columnValue;
    }
}
