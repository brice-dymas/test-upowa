package com.gsb.upowa.util;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * Projet :  upowa
 * Package :  com.gsb.upowa.util
 * File :  EndPointResponse
 * Created on : 2021, Saturday 25 of December
 * Created at : 1:40 PM
 * Author name : Brice dymas
 * Author's mail : briceguemkam@gmail.com / brice.guemkam@iforce5.com
 */
@Builder
@Getter
@Setter
public class EndPointResponse {
    boolean success;
    String message;
    Object data;

    public static EndPointResponse buildSuccessResponse(Object data) {
        return EndPointResponse.builder()
            .success(true)
            .message("Operation effectuee avec succes")
            .data(data)
            .build();
    }

    public static EndPointResponse buildErrorResponse(String message, Object data) {
        return EndPointResponse.builder()
            .success(false)
            .message(message)
            .data(data)
            .build();
    }
}
