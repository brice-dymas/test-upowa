package com.gsb.upowa.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Projet :  upowa
 * Package :  com.gsb.upowa.services
 * File :  CommonCrudService
 * Created on : 2021, Saturday 25 of December
 * Created at : 12:51 PM
 * Author name : Brice dymas
 * Author's mail : briceguemkam@gmail.com / brice.guemkam@iforce5.com
 */
public interface CommonCrudService<E> {

    boolean existsById(Long id);

    E save(E toSave);

    E findById(Long id);

    void deleteById(Long id);

    List<E> findAll();

    Page<E> findAll(Pageable pageable);
}
