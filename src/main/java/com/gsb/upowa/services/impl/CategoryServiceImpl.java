package com.gsb.upowa.services.impl;

import com.gsb.upowa.model.Category;
import com.gsb.upowa.repository.CategoryRepository;
import com.gsb.upowa.services.CommonCrudServiceImpl;
import com.gsb.upowa.services.inter.CategoryService;
import org.springframework.stereotype.Service;

/**
 * Projet :  upowa
 * Package :  com.gsb.upowa.services.impl
 * File :  CategoryServiceImpl
 * Created on : 2021, Saturday 25 of December
 * Created at : 1:07 PM
 * Author name : Brice dymas
 * Author's mail : briceguemkam@gmail.com / brice.guemkam@iforce5.com
 */
@Service
public class CategoryServiceImpl extends CommonCrudServiceImpl<CategoryRepository, Category> implements CategoryService {
    public CategoryServiceImpl(CategoryRepository repository) {
        super(repository);
    }
}
