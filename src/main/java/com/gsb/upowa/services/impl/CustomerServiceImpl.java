package com.gsb.upowa.services.impl;

import com.gsb.upowa.dto.ListCustomerDTO;
import com.gsb.upowa.model.Customer;
import com.gsb.upowa.repository.CustomerRepository;
import com.gsb.upowa.services.CommonCrudServiceImpl;
import com.gsb.upowa.services.inter.CustomerService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Projet :  upowa
 * Package :  com.gsb.upowa.services.impl
 * File :  CustomerServiceImpl
 * Created on : 2021, Saturday 25 of December
 * Created at : 1:08 PM
 * Author name : Brice dymas
 * Author's mail : briceguemkam@gmail.com / brice.guemkam@iforce5.com
 */
@Service
public class CustomerServiceImpl extends CommonCrudServiceImpl<CustomerRepository, Customer> implements CustomerService {
    public CustomerServiceImpl(CustomerRepository repository) {
        super(repository);
    }

    /**
     * Checks if  given ContractNumber already exists in DB
     *
     * @param contractNumber value to check
     * @return true if exists or false otherwise
     */
    @Override
    public boolean existsByContractNumber(String contractNumber) {
        return false;
    }

    /**
     * retourne une liste personalisee de Customers
     */
    @Override
    public List<ListCustomerDTO> getAllCustomers() {
        return repository.getAllCustomers();
    }
}
