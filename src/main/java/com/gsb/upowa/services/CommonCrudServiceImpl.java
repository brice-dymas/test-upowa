package com.gsb.upowa.services;

import com.gsb.upowa.exception.ResourceNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Projet :  upowa
 * Package :  com.gsb.upowa.services
 * File :  CommonCrudServiceImpl
 * Created on : 2021, Saturday 25 of December
 * Created at : 12:54 PM
 * Author name : Brice dymas
 * Author's mail : briceguemkam@gmail.com / brice.guemkam@iforce5.com
 */
@RequiredArgsConstructor
public class CommonCrudServiceImpl<R extends JpaRepository<E, Long>, E> implements CommonCrudService<E> {

    protected final R repository;

    @Override
    public boolean existsById(Long id) {
        return repository.existsById(id);
    }

    @Override
    @Transactional
    public E save(E toSave) {
        return repository.save(toSave);
    }

    @Override
    public E findById(Long id) {
        return repository.findById(id)
            .orElseThrow(() -> new ResourceNotFoundException(this.getClass().getSimpleName(), "id", id));
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    public Page<E> findAll(Pageable pageable) {
        return repository.findAll(pageable);
    }
}
