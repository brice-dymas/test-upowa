package com.gsb.upowa.services.inter;

import com.gsb.upowa.model.Category;
import com.gsb.upowa.services.CommonCrudService;

/**
 * Projet :  upowa
 * Package :  com.gsb.upowa.services.inter
 * File :  CategoryService
 * Created on : 2021, Saturday 25 of December
 * Created at : 1:06 PM
 * Author name : Brice dymas
 * Author's mail : briceguemkam@gmail.com / brice.guemkam@iforce5.com
 */
public interface CategoryService extends CommonCrudService<Category> {
}
