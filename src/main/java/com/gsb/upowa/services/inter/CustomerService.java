package com.gsb.upowa.services.inter;

import com.gsb.upowa.dto.ListCustomerDTO;
import com.gsb.upowa.model.Customer;
import com.gsb.upowa.services.CommonCrudService;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * Projet :  upowa
 * Package :  com.gsb.upowa.services.inter
 * File :  CustomerService
 * Created on : 2021, Saturday 25 of December
 * Created at : 1:06 PM
 * Author name : Brice dymas
 * Author's mail : briceguemkam@gmail.com / brice.guemkam@iforce5.com
 */
public interface CustomerService extends CommonCrudService<Customer> {

    /**
     * Checks if  given ContractNumber already exists in DB
     *
     * @param contractNumber value to check
     * @return true if exists or false otherwise
     */
    boolean existsByContractNumber(@NotBlank String contractNumber);

    /**
     * retourne une liste personalisee de Customers
     */
    List<ListCustomerDTO> getAllCustomers();
}
