package com.gsb.upowa.dto;

import lombok.*;

import java.time.LocalDateTime;

/**
 * Projet :  upowa
 * Package :  com.gsb.upowa.dto
 * File :  ListCustomerDTO
 * Created on : 2021, Saturday 25 of December
 * Created at : 1:09 PM
 * Author name : Brice dymas
 * Author's mail : briceguemkam@gmail.com / brice.guemkam@iforce5.com
 */
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ListCustomerDTO {
    private String names;
    private String phone;
    private String contractNumber;
    private String route;
    private LocalDateTime createdDate;
}
