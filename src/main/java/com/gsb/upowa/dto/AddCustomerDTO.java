package com.gsb.upowa.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * Projet :  upowa
 * Package :  com.gsb.upowa.dto
 * File :  AddCustomerDTO
 * Created on : 2021, Saturday 25 of December
 * Created at : 1:33 PM
 * Author name : Brice dymas
 * Author's mail : briceguemkam@gmail.com / brice.guemkam@iforce5.com
 */
@Builder
@Getter
@Setter
public class AddCustomerDTO {
    private String names;
    private String phone;
    private String contractNumber;
    private String route;
}
