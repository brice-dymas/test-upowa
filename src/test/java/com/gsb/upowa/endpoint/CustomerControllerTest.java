package com.gsb.upowa.endpoint;

import com.gsb.upowa.dto.ListCustomerDTO;
import com.gsb.upowa.model.Customer;
import com.gsb.upowa.services.inter.CustomerService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Projet :  upowa
 * Package :  com.gsb.upowa.endpoint
 * File :  CustomerControllerTest
 * Created on : 2021, Saturday 25 of December
 * Created at : 2:03 PM
 * Author name : Brice dymas
 * Author's mail : briceguemkam@gmail.com / brice.guemkam@iforce5.com
 */
@SpringBootTest
@DisplayName("Customer Rest Controller Tests")
class CustomerControllerTest {

    private final String NAME = "SANDO";
    private final String CONTRACT_NUMBER = "CA00A";
    private final String PHONE = "693552463";
    private final String ROUTE = "NKOLBISSON";
    @MockBean
    private CustomerService customerService;
    private Customer customer;

    @BeforeEach
    void setUp() {
        System.out.println("setting data before test");
        customer = Customer.builder()
            .contractNumber(CONTRACT_NUMBER)
            .names(NAME)
            .phone(PHONE)
            .route(ROUTE)
            .build();
    }

    @Test
    @DisplayName("Get all customers")
    void getAllCustomers() {
        List<ListCustomerDTO> allCustomers = customerService.getAllCustomers();
        assertTrue(allCustomers.isEmpty());
    }

    @Test
    @DisplayName("Saving new customer")
    void createCustomer() {
        Customer saved = customerService.save(customer);
        assertEquals(NAME, saved.getNames());
        assertEquals(PHONE, saved.getPhone());
        assertEquals(CONTRACT_NUMBER, saved.getContractNumber());
        assertEquals(ROUTE, saved.getRoute());
        assertNotNull(saved.getId());
        assertNotNull(saved.getCreatedDate());
    }
}
