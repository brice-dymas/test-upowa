package com.gsb.upowa.endpoint;

import com.gsb.upowa.services.inter.CustomerService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Projet :  upowa
 * Package :  com.gsb.upowa.endpoint
 * File :  CustomerControllertestv2
 * Created on : 2021, Saturday 25 of December
 * Created at : 2:25 PM
 * Author name : Brice dymas
 * Author's mail : briceguemkam@gmail.com / brice.guemkam@iforce5.com
 */
@WebMvcTest(CustomerController.class)
public class CustomerControllertestv2 {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    private CustomerService customerService;

    @Test
    void testGetAllCustomers() throws Exception {
        mockMvc.perform(get("/api/customer/list"))
            .andDo(print())
            .andExpect(status().isOk());
    }
}
